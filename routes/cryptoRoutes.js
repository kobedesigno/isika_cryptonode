const express = require('express');
const router = express.Router();
const cryptos = require('../controllers/cryptoController');
// const auth = require('../middleware/auth');
// const multer = require('../middleware/multer-config');


router.get("/", cryptos.getAllCrypto);
router.get("/:id", cryptos.getOneById);
router.get("/dateName/:date/:cryptoName", cryptos.getOneCryptoByNameAndDate);
router.get("/date/:date", cryptos.getAllCryptoByDate);
router.get("/cryptoName/:cryptoName", cryptos.getOneCryptoByName);

module.exports = router;
