const Crypto = require('../models/cryptoModel');
const fs = require('fs');

exports.getAllCrypto = (req, res, next) => {
    Crypto.find()
        .then(cryptos => res.status(200).json(cryptos))
        .catch(error => res.status(400).json({ error }));
};

exports.getAllCryptoByDate = (req, res, next) => {
    Crypto.find({ date: req.params.date })
        .then(cryptos => res.status(200).json(cryptos))
        .catch(error => res.status(400).json({ error }));
};

exports.getOneCryptoByName = (req, res, next) => {
    Crypto.find({ cryptoName: req.params.cryptoName})
        .then(cryptos => res.status(200).json(cryptos))
        .catch(error => res.status(400).json({ error }));
};

exports.getOneCryptoByNameAndDate = (req, res, next) => {
    Crypto.find({ date: req.params.date , cryptoName: req.params.cryptoName})
        .then(cryptos => res.status(200).json(cryptos))
        .catch(error => res.status(400).json({ error }));
};

exports.getOneById = (req, res, next) => {
    Crypto.findOne({ _id: req.params.id })
        .then((crypto) => {
          console.log(crypto);
          res.status(200).json(crypto)
        })
        .catch(error => res.status(404).json({ error }));
};

